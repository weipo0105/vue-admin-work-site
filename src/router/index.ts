import {
  RouteRecordRaw,
  createRouter,
  createWebHistory,
  createWebHashHistory,
} from 'vue-router'
import Home from '../pages/index.vue'
import Guide from '../pages/guide.vue'
import About from '../pages/about.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/index',
  },
  {
    path: '/index',
    name: 'index',
    component: Home,
  },
  {
    path: '/guide',
    name: 'guide',
    component: Guide,
    redirect: '/guide/introduce',
    children: [
      {
        path: 'introduce',
        component: () => import('../pages/guide/introduce.vue'),
      },
      {
        path: 'start',
        component: () => import('../pages/guide/start.vue'),
      },
      {
        path: 'setting',
        component: () => import('../pages/guide/setting.vue'),
      },
      {
        path: 'router',
        component: () => import('../pages/guide/router.vue'),
      },
      {
        path: 'permission',
        component: () => import('../pages/guide/permission.vue'),
      },
      {
        path: 'component',
        component: () => import('../pages/guide/component.vue'),
      },
      {
        path: 'style',
        component: () => import('../pages/guide/style.vue'),
      },
      {
        path: 'mock',
        component: () => import('../pages/guide/mock.vue'),
      },
      {
        path: 'build',
        component: () => import('../pages/guide/build.vue'),
      },
      {
        path: 'question',
        component: () => import('../pages/guide/question.vue'),
      },
      {
        path: 'log',
        component: () => import('../pages/guide/log.vue'),
      },
    ],
  },
  {
    path: '/about',
    name: 'about',
    component: About,
  },
  {
    path: '/permission',
    name: 'permission',
    component: () => import('../pages/permission.vue'),
  },
  {
    path: '/question',
    name: 'question',
    component: () => import('../pages/question.vue'),
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
