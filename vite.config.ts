import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import ViteComponents, { NaiveUiResolver } from "vite-plugin-components";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/",
  plugins: [
    vue(),
    ViteComponents({
      customComponentResolvers: NaiveUiResolver(),
    }),
  ],
  server: {
    open: true,
  },
});
